#image from dockerhub, in this case is node with alpine for light size 
FROM node:12.19.0-alpine

#create user for run app
RUN adduser --disabled-password --gecos '' cristhian

#change user
USER cristhian
#RUN echo "the user to run app is:" && whoami

#set workder directory to reto-devops
WORKDIR /opt/reto-devops

#copy app
COPY . .

#run app
EXPOSE 3000
CMD ["npm", "run", "test"]
CMD ["node", "index.js"]
